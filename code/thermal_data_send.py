import socket
import numpy as np
import encodings
import time
import board
import busio
import adafruit_mlx90640

PRINT_TEMPERATURES = False
PRINT_ASCIIART = True
# HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 65432  # Port to listen on (non-privileged ports are > 1023)


def take_termal_frame(mlx):
    frame = [0] * 768
    while True:
        stamp = time.monotonic()
        try:
            mlx.getFrame(frame)
            print("Read 2 frames in %0.2f s" % (time.monotonic() - stamp))
            return frame
        except ValueError:
            # these happen, no biggie - retry
            continue


def random_data():
    x1 = np.random.randint(0, 55, None)  # Dummy temperature
    y1 = np.random.randint(0, 45, None)  # Dummy humidigy
    my_sensor = "{},{}".format(x1, y1)
    return my_sensor  # return data seperated by comma


def my_server():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        print("Server Started waiting for client to connect ")
        # s.bind(('',PORT))
        # s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # s.listen(5)

        s.bind(('', PORT))
        s.listen(5)

        conn, addr = s.accept()

        with conn:
            print('Connected by', addr)
            data = conn.recv(1024).decode('utf-8')
            while True:

                if str(data) == "test":
                    print("test, test, bibup bibup ")

                    my_data = random_data()

                    x_encoded_data = my_data.encode('utf-8')

                    conn.send(x_encoded_data)

                elif str(data) == "termal":
                    # i2c = busio.I2C(board.SCL, board.SDA, frequency=800000)
                    # mlx = adafruit_mlx90640.MLX90640(i2c)
                    # print("MLX addr detected on I2C")
                    # mlx.refresh_rate = adafruit_mlx90640.RefreshRate.REFRESH_2_HZ

                    i2c = busio.I2C(board.SCL, board.SDA, frequency=400000)  # setup I2C
                    mlx = adafruit_mlx90640.MLX90640(i2c)  # begin MLX90640 with I2C comm
                    mlx.refresh_rate = adafruit_mlx90640.RefreshRate.REFRESH_2_HZ

                    while True:
                        frame = take_termal_frame(mlx)
                        frameString = (" " + ' '.join(str(e) for e in frame)).encode('utf-8')

                        conn.send(frameString)
                        time.sleep(0.4)



                elif str(data) == "Quit":
                    print("shutting down server ")
                    break

                else:
                    pass


if __name__ == '__main__':
    while 1:
        my_server()