import socket
from scipy import signal
import threading
import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import ndimage
from datetime import datetime
import os
import select
import sys


HOST = '192.168.1.31'  # The server's hostname or IP address
#192.168.1.31
#'192.168.178.38'
PORT = 65432        # The port used by the server


def plot_termal(fig, ax, therm1, data_array, meanTemperature, maxTemperature, max2x2Temperature,
                simple_threshold, upper_IQR_threhold, z_score):
    therm1.set_data(data_array) # flip left to right
    #cbar.on_mappable_changed(therm1)  # update colorbar range "Current Temperature: "
    if max2x2Temperature < z_score:
        fever_detected = 'No Fever Detected'
    else:
        fever_detected = 'Fever Detected !!!'

    plt.figtext(0.45, 0.92,  fever_detected, fontsize=28,
                bbox={"facecolor": "green", "alpha": 0.5, "pad": 5})

    plt.figtext(0.02, 0.80, 'Measured Temperatures', fontsize=22,
                bbox={"facecolor": "orange", "alpha": 0.5, "pad": 5})
    plt.figtext(0.02, 0.75,'mean '+str(np.round(meanTemperature, 2)), fontsize=18,
                bbox={"facecolor": "orange", "alpha": 0.5, "pad": 5})
    plt.figtext(0.02, 0.70, 'max '+str(np.round(maxTemperature, 2)), fontsize=18,
                bbox={"facecolor": "orange", "alpha": 0.5, "pad": 5})
    plt.figtext(0.02, 0.65, '2x2 max ' + str(np.round(max2x2Temperature, 2)), fontsize=18,
                bbox={"facecolor": "orange", "alpha": 0.5, "pad": 5})

    plt.figtext(0.02, 0.45, 'Calculated Fever Thresholds \n (based on 2x2 max)', fontsize=22,
                bbox={"facecolor": "orange", "alpha": 0.5, "pad": 5})
    plt.figtext(0.02, 0.40, 'Simple Threhold: ' + str(np.round(simple_threshold, 2)), fontsize=18,
                bbox={"facecolor": "orange", "alpha": 0.5, "pad": 5})
    plt.figtext(0.02, 0.35, 'IQR Threshold: ' + str(np.round(upper_IQR_threhold, 2)), fontsize=18,
                bbox={"facecolor": "orange", "alpha": 0.5, "pad": 5})
    plt.figtext(0.02, 0.30, 'Z-Score Threshold: ' + str(np.round(z_score, 2)), fontsize=18,
                bbox={"facecolor": "orange", "alpha": 0.5, "pad": 5})


    plt.pause(0.1)  # required




def estimateTemperature(data_array, lower, upper):
    mean_filter = np.array([[0.25, 0.25], [0.25, 0.25]])
    mean_conv = signal.convolve2d(data_array, mean_filter, boundary='symm', mode='same')
    temperature = data_array[(data_array > 30) & (data_array < 39)]
    temperature_conv = mean_conv[(mean_conv > 30) & (mean_conv < 39)]

    print('selected pixels', np.shape(temperature))
    if len(temperature) > 0:
        meanTemperature = np.mean(np.mean(temperature))
        maxTemperature = np.max(np.max(temperature))
        max2x2Temperature = np.max(np.max(temperature_conv))
    else:
        meanTemperature = -1
        maxTemperature = -1
        max2x2Temperature = -1
    return meanTemperature, maxTemperature, max2x2Temperature


def average_max_4_values(df_series):
    max2x2 = df_series.array
    average_filter = np.array([0.25, 0.25, 0.25, 0.25])
    averaged_max4 = signal.convolve(max2x2, average_filter, mode='same')
    temp_max = max(averaged_max4)
    return temp_max

def calc_fever_thresholds(path, temp_series):
    #read in historical data
    dir_lst = [el for el in os.listdir(path) if el[-4:] == '.csv' and el[-11:] != 'results.csv']
    results_data = []
    for el in dir_lst:
        df_temp = pd.read_csv(path + el)
        temp_mean = average_max_4_values(df_temp.mean_temperature)
        temp_max = average_max_4_values(df_temp.max_temperature)
        temp_max4x4 = average_max_4_values(df_temp.max_2x2_temperature)

        results_data.append([el, temp_mean, temp_max, temp_max4x4])

    df_results = pd.DataFrame(data=results_data, columns=['file_name', 'mean', 'max', 'max4x4'])

    #calculate the scores
    avg_IQR = df_results[temp_series].quantile(.75)-df_results[temp_series].quantile(.25)
    simple_threshold = df_results[temp_series].mean()+2
    upper_IQR_threhold = df_results[temp_series].quantile(.75)+1.5*avg_IQR
    z_score = df_results[temp_series].mean() + df_results[temp_series].std() * 2

    return simple_threshold, upper_IQR_threhold, z_score



def my_client(pathOut):



    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:

        s.connect((HOST, PORT))

        my = "termal"
        my_inp = my.encode('utf-8')
        s.sendall(my_inp)



        plt.ion()  # enables interactive plotting
        frame_shape = (24, 32)  # mlx90640 shape

        fig = plt.figure(figsize=(18, 12))  # start figure
        ax = fig.add_subplot(111)  # add subplot
        fig.subplots_adjust(right = 1)


        therm1 = ax.imshow(np.zeros(frame_shape), interpolation='none',
                            cmap='hot', vmin=25, vmax=40)  # preemptive image cmap=plt.cm.bwr,

        cbar = fig.colorbar(therm1)  # setup colorbar
        cbar.set_label('Temperature [$^{\circ}$C]', fontsize=14)  # colorbar label

        #I need both arrays!
        data_list = []
        dataList = []
        n=0
        try:
            while True:
                data = s.recv(16384).decode('utf-8')

                tempdata = [float(temp) for temp in data.split()]
                dataList+=tempdata

                print(len(dataList))

                if len(dataList) >=768:
                    print("full msg recvd")
                    frame = dataList[:768]
                    #print(frame)
                    dataList = []
                    frame = np.reshape(np.asarray(frame),frame_shape)
                    data_array = np.reshape(frame, frame_shape)  # reshape, maybe still flip data



                    #temperature
                    print('data frame', np.shape(data_array)[0]*np.shape(data_array)[1])

                    #print(temperature)
                    meanTemperature, maxTemperature, max2x2Temperature = estimateTemperature(data_array, 30, 40)

                    print('mean temperature: ', meanTemperature)
                    print('mean temperature: ', maxTemperature)

                    #calc the fever threshold
                    simple_threshold, upper_IQR_threhold, z_score = calc_fever_thresholds(pathOut, 'max4x4')

                    #plot results
                    plot_termal(fig, ax, therm1, data_array,meanTemperature, maxTemperature, max2x2Temperature,
                                simple_threshold, upper_IQR_threhold, z_score)

                    plt.savefig('/home/nulpe/Desktop/fever_detection/data/images/example'+str(n)+'.png')
                    n=n+1



                    #save df
                    dateTimeObj = datetime.now()
                    time_stamp = str(dateTimeObj.year)+ '/'+str(dateTimeObj.month)+'/'+str(dateTimeObj.day)+'/'+\
                                 str(dateTimeObj.hour)+':'+ str(dateTimeObj.minute) +str(dateTimeObj.second)
                    data_list.append([time_stamp, meanTemperature, maxTemperature, max2x2Temperature])

                time.sleep(0.4)


        except:
            # Define dataframe to save to csv
            col_names = ['time_stamp', 'mean_temperature', 'max_temperature', 'max_2x2_temperature']

            df_temperature = pd.DataFrame(data=data_list, columns=col_names)
            df_temperature.index = df_temperature.time_stamp
            path = '/home/nulpe/Desktop/fever_detection/data/temperature_data/'
            dir_lst = [el for el in os.listdir(path) if el[-4:]=='.csv' and el[-11:]!='results.csv']
            print('saved to data frame')
            df_temperature.to_csv(path+str(len(dir_lst))+'_temperature_session.csv')
            #You save empty rows of data, this is shit look at that



            #calculate outlier metrics
            results_data = []
            for el in dir_lst:
                df_temp = pd.read_csv(path + el)
                temp_mean = average_max_4_values(df_temp.mean_temperature)
                temp_max = average_max_4_values(df_temp.max_temperature)
                temp_max4x4 = average_max_4_values(df_temp.max_2x2_temperature)

                results_data.append([el, temp_mean, temp_max, temp_max4x4])

            df_results = pd.DataFrame(data=results_data, columns=['file_name', 'mean', 'max', 'max4x4'])
            print('n observations is ', len(df_results))
            print('avg mean ', df_results['mean'].mean(), ' standard deviation ', df_results['mean'].std())
            print('max mean ', df_results['max'].mean(), ' standard deviation ', df_results['max'].std())
            print('max4x4 mean ', df_results['max4x4'].mean(), ' standard deviation ', df_results['max4x4'].std())

            df_results.to_csv(path + 'aggregated_temperature_results.csv')


            sys.exit()





if __name__ == "__main__":
    print('running...')
    pathOut = '/home/nulpe/Desktop/fever_detection/data/temperature_data/'
    while 1:
        my_client(pathOut)