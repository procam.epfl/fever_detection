import pandas as pd
import numpy as np
from scipy import signal
import os


def average_max_4_values(df_series):
    max2x2 = df_series.array
    average_filter = np.array([0.25, 0.25, 0.25, 0.25])
    averaged_max4 = signal.convolve(max2x2, average_filter, mode='same')
    temp_max = max(averaged_max4)
    return temp_max

def calc_fever_thresholds(temp_series):
    avg_IQR = df_results[temp_series].quantile(.75)-df_results[temp_series].quantile(.25)
    print(temp_series+' avg ', df_results[temp_series].mean(), ' standard deviation ', df_results[temp_series].std(), 'IQR ', avg_IQR)
    print(temp_series+' simple threshold ', df_results[temp_series].mean()+2, 'z score ',  df_results[temp_series].mean()+df_results[temp_series].std()*2,
          'upper IQR threhold', df_results[temp_series].quantile(.75)+1.5*avg_IQR)
    print('\n')

path = '/home/nulpe/Desktop/fever_detection/data/temperature_data/'
dir_lst = [el for el in os.listdir(path) if el[-4:]=='.csv' and el[-11:]!='results.csv']

results_data = []
for el in dir_lst:
    df_temp = pd.read_csv(path + el)
    temp_mean = average_max_4_values(df_temp.mean_temperature)
    temp_max = average_max_4_values(df_temp.max_temperature)
    temp_max4x4 = average_max_4_values(df_temp.max_2x2_temperature)

    results_data.append([el, temp_mean, temp_max, temp_max4x4])

df_results = pd.DataFrame(data=results_data, columns=['file_name', 'mean', 'max', 'max4x4'])
print('n observations is ', len(df_results))



calc_fever_thresholds('mean')
calc_fever_thresholds('max')
calc_fever_thresholds('max4x4')

df_results.to_csv(path + 'aggregated_temperature_results.csv')